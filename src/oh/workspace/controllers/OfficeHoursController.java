package oh.workspace.controllers;

import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.util.Comparator;
import java.util.Iterator;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_FOOLPROOF_SETTINGS;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_CONTENT;
import static oh.OfficeHoursPropertyType.OH_NO_TA_SELECTED_TITLE;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.data.TimeSlot.DayOfWeek;
import oh.transactions.AddTA_Transaction;
import oh.transactions.ToggleOfficeHours_Transaction;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import oh.transactions.ChangeTA_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursController {

    OfficeHoursApp app;
    private static boolean isTypeAll;
    private static boolean isGradMode;

    public OfficeHoursController(OfficeHoursApp initApp) {
        app = initApp;
        isTypeAll = true;
        isGradMode = false;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        if (isTypeAll){
            Stage window = app.getGUIModule().getWindow();
            AppDialogsFacade.showMessageDialog(window, "Warn", "Undergraduate or Graduate must be selected.");
            return;
        }
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        if (data.isLegalNewTA(name, email, isGradMode)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), isGradMode);
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
    }
    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta, this);
                    app.processTransaction(transaction);
                }
                else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }
    
    public void processToggleOfficeHoursDouble(){
        AppGUIModule gui = app.getGUIModule();
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        EditTAWindow.display(ta);
        if (EditTAWindow.confirmed){
            if (! data.isLegalNewTA(EditTAWindow.newName, EditTAWindow.newEmail, EditTAWindow.newIsGrad)){
                return;
            }
            ChangeTA_Transaction transaction = new ChangeTA_Transaction(data, ta, EditTAWindow.newName, EditTAWindow.newEmail, EditTAWindow.newIsGrad, this);
            app.processTransaction(transaction);
        }
    }

    public void processTypeTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        // Red Error Feedback for Add TA text fields
        TeachingAssistantPrototype testExistTA = data.getTAWithName(name);
        if (testExistTA != null && testExistTA.isGrad() == isGradMode){
            nameTF.setStyle("-fx-text-inner-color: red;");
        } else {
            nameTF.setStyle("-fx-text-inner-color: black;");
        }
        if (! data.isLegalNewEmail(email)){
            emailTF.setStyle("-fx-text-inner-color: red;");
        } else {
            emailTF.setStyle("-fx-text-inner-color: black;");
        }
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }
    
    public void changeCellColor(){
        AppGUIModule gui = app.getGUIModule();
        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        
        for (Node n: officeHoursTableView.lookupAll("TableRow")) {
            TableRow row = (TableRow) n;
            TimeSlot slot = (TimeSlot) row.getItem();
            if (slot != null && slot.contains(ta)){
                row.setStyle("-fx-background-color: grey;");
            } else {
                row.setStyle("-fx-background-color: white;");
            }
        }        
    }
    
    public void processTAClicked(){
        app.getFoolproofModule().updateAll();
    }
    
    public static boolean getIsGradMode(){
        return isGradMode;
    }
    public static boolean getIsTypeALl(){
        return isTypeAll;
    }
    
    public void processTypeChange(ToggleGroup group){
        String selectedToggle = group.getSelectedToggle().toString();
        if (selectedToggle.contains("All")){
            processTypeChangeAll();
        } else if (selectedToggle.contains("Undergraduate")){
            processTypeChangeUndergrad();
        } else if (selectedToggle.contains("Graduate")){
            processTypeChangeGrad();
        }
    }
    
    public void sortTable(){
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        
        taTableView.getItems().sort(new TAComparator());
        taTableView.refresh();
    }
    
    private void processTypeChangeAll(){
        isTypeAll = true;
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        
        taTableView.setItems(data.teachingAssistants);
        officeHoursTableView.setItems(data.officeHours);
        
        taTableView.getItems().sort(new TAComparator());
        officeHoursTableView.refresh();
        taTableView.refresh();
    }
    
    private void processTypeChangeUndergrad(){
        isTypeAll = false;
        isGradMode = false;
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        
        taTableView.setItems(data.teachingAssistantsUnder);
        
        taTableView.getItems().sort(new TAComparator());
        officeHoursTableView.setItems(data.officeHoursUnder);
        officeHoursTableView.refresh();
        taTableView.refresh();
    }
    
    private void processTypeChangeGrad(){
        isTypeAll = false;
        isGradMode = true;
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        OfficeHoursData data = (OfficeHoursData)app.getDataComponent();
        
        taTableView.setItems(data.teachingAssistantsGrad);
        
        taTableView.getItems().sort(new TAComparator());
        officeHoursTableView.setItems(data.officeHoursGrad);
        officeHoursTableView.refresh();
        taTableView.refresh();
    }
    
    public void refreshTableView(){
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        officeHoursTableView.refresh();
        taTableView.refresh();
    }
}
