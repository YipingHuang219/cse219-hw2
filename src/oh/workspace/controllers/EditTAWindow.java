package oh.workspace.controllers;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import oh.data.TeachingAssistantPrototype;

/**
 * a new window to prompt player to confirm information, when the confirm box
 * is created, player must to click yes or no in order to continue
 */
class EditTAWindow {
    static String newName;
    static String newEmail;
    static Boolean newIsGrad;
    static boolean confirmed;
    
    /**
     * create and display the confirm box
     */
    static void display(TeachingAssistantPrototype ta){
        newName = null;
        newEmail = null;
        newIsGrad = null;
        
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("");

        Text messageText = new Text("Edit Teaching Assistant");
        messageText.setFont(new Font(20));
        messageText.setStyle("-fx-font-weight: bold");
        Text nameText = new Text("Name: ");
        nameText.setFont(new Font(15));
        nameText.setStyle("-fx-font-weight: bold");
        Text emailText = new Text("Email: ");
        emailText.setFont(new Font(15));
        emailText.setStyle("-fx-font-weight: bold");
        Text typeText = new Text("Type: ");
        typeText.setFont(new Font(15));
        typeText.setStyle("-fx-font-weight: bold");
        RadioButton radioUnder = new RadioButton("Undergraduate");
        radioUnder.setFont(new Font(15));
        radioUnder.setStyle("-fx-font-weight: bold");
        RadioButton radioGrad = new RadioButton("Graduate");
        radioGrad.setFont(new Font(15));
        radioGrad.setStyle("-fx-font-weight: bold");
        ToggleGroup group = new ToggleGroup();
        TextField nameTF = new TextField(ta.getName());
        nameTF.setFont(new Font(15));
        nameTF.setStyle("-fx-font-weight: bold");
        TextField emailTF = new TextField(ta.getEmail());
        emailTF.setFont(new Font(15));
        emailTF.setStyle("-fx-font-weight: bold");
        Button yesButton = new Button("Ok");
        yesButton.setFont(new Font(15));
        yesButton.setStyle("-fx-font-weight: bold");
        Button noButton = new Button("Cancel");
        noButton.setFont(new Font(15));
        noButton.setStyle("-fx-font-weight: bold");

        radioUnder.setSelected(!ta.isGrad());
        radioGrad.setSelected(ta.isGrad());
        radioUnder.setToggleGroup(group);
        radioGrad.setToggleGroup(group);
        
        yesButton.setOnAction(event -> {
            confirmed = true;
            newName = nameTF.getText();
            newEmail = emailTF.getText();
            newIsGrad = radioGrad.isSelected();
            window.close();
        });

        noButton.setOnAction(event -> {
            confirmed = false;
            window.close();
        });

        VBox layout = new VBox();
        HBox nameBox = new HBox();
        HBox emailBox = new HBox();
        HBox typeBox = new HBox();
        HBox buttons = new HBox();
        
        buttons.setAlignment(Pos.CENTER_LEFT);
        buttons.setSpacing(0);
        buttons.setPadding(new Insets(20, 0, 0, 0));

        layout.setAlignment(Pos.CENTER_LEFT);
        layout.setSpacing(10);
        layout.setPadding(new Insets(10, 10, 10, 10));
        
        nameBox.setAlignment(Pos.CENTER_LEFT);
        nameBox.setSpacing(10);
        
        emailBox.setAlignment(Pos.CENTER_LEFT);
        emailBox.setSpacing(10);
        
        typeBox.setAlignment(Pos.CENTER_LEFT);
        typeBox.setSpacing(10);

        nameBox.getChildren().addAll(nameText, nameTF);
        emailBox.getChildren().addAll(emailText, emailTF);
        typeBox.getChildren().addAll(typeText, radioGrad, radioUnder);
        buttons.getChildren().addAll(yesButton, noButton);
        layout.getChildren().addAll(messageText, nameBox, emailBox, typeBox, buttons);

        window.setMinHeight(250);
        window.setMinWidth(350);
        window.setScene(new Scene(layout));
        window.showAndWait();
    }
}
