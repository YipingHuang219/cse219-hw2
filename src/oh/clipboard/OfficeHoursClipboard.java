package oh.clipboard;

import djf.components.AppClipboardComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.transactions.AddTA_Transaction;
import oh.workspace.controllers.OfficeHoursController;
import oh.workspace.controllers.TAComparator;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursClipboard implements AppClipboardComponent {
    OfficeHoursApp app;
    ArrayList<TeachingAssistantPrototype> clipboardCutItems;
    ArrayList<TeachingAssistantPrototype> clipboardCopiedItems;
    TeachingAssistantPrototype clipboardTA;
    TableView<TeachingAssistantPrototype> taTableView;
    OfficeHoursData data;
    
    public OfficeHoursClipboard(OfficeHoursApp initApp) {
        app = initApp;
        AppGUIModule gui = app.getGUIModule();
        taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        data = (OfficeHoursData)app.getDataComponent();
        clipboardCutItems = null;
        clipboardCopiedItems = null;
    }
    
    @Override
    public void cut() {
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        clipboardTA = new TeachingAssistantPrototype(ta);
        data.removeTA(ta);
        app.getFoolproofModule().updateAll();
    }

    @Override
    public void copy() {
        TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
        clipboardTA = new TeachingAssistantPrototype(ta);
        app.getFoolproofModule().updateAll();
    }
    
    @Override
    public void paste() {
        if (OfficeHoursController.getIsTypeALl()){
            return;
        }
        if (data.isLegalNewTA(clipboardTA.getName(), clipboardTA.getEmail(), clipboardTA.isGrad())) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(clipboardTA.getName(), clipboardTA.getEmail(), OfficeHoursController.getIsGradMode());
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);
        } else {
            int i = 0;
            String newName, newEmail;
            do {
                newName = clipboardTA.getName() + i;
                String[] emailSplit = clipboardTA.getEmail().split("@");
                newEmail = emailSplit[0] + i + "@" + emailSplit[1];
                ++i;
            } while (!data.isLegalNewTA(newName, newEmail, OfficeHoursController.getIsGradMode()));
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(newName, newEmail, OfficeHoursController.getIsGradMode());
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);
        }
        clipboardTA = null;
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        
        taTableView.getItems().sort(new TAComparator());
        taTableView.refresh();
        app.getFoolproofModule().updateAll();
    }    


    @Override
    public boolean hasSomethingToCut() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        return !OfficeHoursController.getIsTypeALl() && clipboardTA != null;
    }
}