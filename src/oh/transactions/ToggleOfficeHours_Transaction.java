package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.data.TimeSlot;
import oh.data.TimeSlot.DayOfWeek;
import oh.workspace.controllers.OfficeHoursController;

/**
 *
 * @author McKillaGorilla
 */
public class ToggleOfficeHours_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TimeSlot timeSlot, timeSlotUnder, timeSlotGrad;
    DayOfWeek dow;
    TeachingAssistantPrototype ta;
    OfficeHoursController controller;
    
    public ToggleOfficeHours_Transaction(   OfficeHoursData initData, 
                                            TimeSlot initTimeSlot,
                                            DayOfWeek initDOW,
                                            TeachingAssistantPrototype initTA,
                                            OfficeHoursController controller) {
        data = initData;
        timeSlot = initTimeSlot;
        timeSlotUnder = data.getTimeSlotUnder(initTimeSlot.getStartTime());
        timeSlotGrad = data.getTimeSlotGrad(initTimeSlot.getStartTime());
        
        dow = initDOW;
        ta = initTA;
        this.controller = controller;
    }

    @Override
    public void doTransaction() {
        try {
            timeSlot.toggleTA(dow, ta);
            if (!ta.isGrad()){
                timeSlotUnder.toggleTA(dow, ta);
            } else {
                timeSlotGrad.toggleTA(dow, ta);
            }
            controller.changeCellColor();
            controller.refreshTableView();
        } catch (NullPointerException ignored){}
    }

    @Override
    public void undoTransaction() {
        try {
            timeSlot.toggleTA(dow, ta);
            if (!ta.isGrad()){
                timeSlotUnder.toggleTA(dow, ta);
            } else {
                timeSlotGrad.toggleTA(dow, ta);
            }
            controller.changeCellColor();
            controller.refreshTableView();
        } catch (NullPointerException ignored){}
    }
}