package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.workspace.controllers.OfficeHoursController;

/**
 *
 * @author McKillaGorilla
 */
public class ChangeTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype ta;
    String oldName, newName;
    String oldEmail, newEmail;
    boolean oldIsGrad, newIsGrad;
    OfficeHoursController controller;
    
    public ChangeTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA, String newName, String newEmail, boolean newIsGrad, OfficeHoursController controller) {
        data = initData;
        ta = initTA;
        oldName = ta.getName();
        oldEmail = ta.getEmail();
        oldIsGrad = ta.isGrad();
        this.newName = newName;
        this.newEmail = newEmail;
        this.newIsGrad = newIsGrad;
        this.controller = controller;
    }

    @Override
    public void doTransaction() {
        ta.setName(newName);
        ta.setEmail(newEmail);
        ta.setType(newIsGrad);
        if (oldIsGrad && !newIsGrad){
            data.teachingAssistantsGrad.remove(ta);
            data.teachingAssistantsUnder.add(ta);
        } else if (!oldIsGrad && newIsGrad){
            data.teachingAssistantsGrad.add(ta);
            data.teachingAssistantsUnder.remove(ta);
        }
        controller.sortTable();
        controller.refreshTableView();
    }

    @Override
    public void undoTransaction() {
        ta.setName(oldName);
        ta.setEmail(oldEmail);
        ta.setType(oldIsGrad);
        if (!oldIsGrad && newIsGrad){
            data.teachingAssistantsGrad.remove(ta);
            data.teachingAssistantsUnder.add(ta);
        } else if (oldIsGrad && !newIsGrad){
            data.teachingAssistantsGrad.add(ta);
            data.teachingAssistantsUnder.remove(ta);
        }
        controller.sortTable();
        controller.refreshTableView();
    }
}
